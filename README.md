# 运行步骤

启动 com.lagou.dubbo.demo.Demo8081Application

com.lagou.dubbo.demo.Demo8082Application

com.lagou.dubbo.demo.Demo8083Application

**之后再启动**

com.lagou.dubbo.web.MainApplicaiton

注意MainApplicaiton依赖前面三个项目

# 工程结构

rpc-dubbo-8081: 第一个作业打印ip例子

rpc-dubbo-8082: 第二个作业打印ip例子

rpc-dubbo-8083: 第三个作业随机休眠service

rpc-dubbo-web: web项目

# 访问地址

http://127.0.0.1:8080/test

访问之后，在Demo8081Application控制台会打印以下内容：

```
2020-04-14 23:08:24.901  INFO 14704 --- [:20880-thread-5] c.lagou.dubbo.demo.service.DemoService   : 客户端请求ip为:192.168.3.29
```

http://127.0.0.1:8080/monitor

访问之后，在WebApplication控制台，会打印下面内容：

```
2020-04-14 23:13:21.123  INFO 19020 --- [pool-7-thread-1] com.lagou.dubbo.web.util.MonitorBean     : methodA等待下一次调度....:12,队列长度:1102
2020-04-14 23:13:26.125  INFO 19020 --- [pool-7-thread-1] com.lagou.dubbo.web.util.MonitorBean     : methodA tp90:45,队列长度:1193
```

# 备注

所有作业使用spring boot构建

# 运行截图

## 打印ip

![](image\打印ip截图.png)

## 性能监控

![image-20200415231351137](image\image-20200415231351137.png)