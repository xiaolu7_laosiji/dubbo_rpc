package com.lagou.dubbo.web.controller;

import com.lagou.dubbo.demo.service.DemoService;
import com.lagou.dubbo.demo.service.MonitorService;
import com.lagou.dubbo.web.util.TransportIpFilter;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 测试用
 */
@RestController
public class TestController {
    @Reference
    private DemoService demoService;
    @Reference(timeout = 3000)
    private MonitorService monitorService;

    @RequestMapping(value = "/test", produces = "application/json")
    public boolean demo(HttpServletRequest request) {
        TransportIpFilter.setRequest(request);
        demoService.printIp();
        return true;
    }

    @RequestMapping("/monitor")
    public boolean monitor() {
        //ExecutorService pool = Executors.newWorkStealingPool();
        new Thread(() -> {
            while(true) {
                monitorService.methodA();
                //monitorService.methodB();
            }
        }).start();

        return true;
    }
}
