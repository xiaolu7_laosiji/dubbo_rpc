# spi

## 1.简介

SPI 全称为 (Service Provider Interface) ，是JDK内置的一种服务提供发现机制。 目前有不少框架用它
来做服务的扩展发现，简单来说，它就是一种动态替换发现的机制。使用SPI机制的优势是实现解耦，
使得第三方服务模块的装配控制逻辑与调用者的业务代码分离

Java中如果想要使用SPI功能，先提供标准服务接口，然后再提供相关接口实现和调用者。这样就可以通
过SPI机制中约定好的信息进行查询相应的接口实现。
SPI遵循如下约定：
1、当服务提供者提供了接口的一种具体实现后，在META-INF/services目录下创建一个以“接口全
限定名”为命名的文件，内容为实现类的全限定名；
2、接口实现类所在的jar包放在主程序的classpath中；
3、主程序通过java.util.ServiceLoader动态装载实现模块，它通过扫描META-INF/services目录下
的配置文件找到实现类的全限定名，把类加载到JVM；
4、SPI的实现类必须携带一个无参构造方法；

## 2.dubbo中的spi

dubbo中大量的使用了SPI来作为扩展点，通过实现同一接口的前提下，可以进行定制自己的实现类。
比如比较常见的协议，负载均衡，都可以通过SPI的方式进行定制化，自己扩展。Dubbo中已经存在的
所有已经实现好的扩展点。

**自带扩展点**

**/META-INF/dubbo/internal**

**dubbo自己做SPI的目的**

1. JDK 标准的 SPI 会一次性实例化扩展点所有实现，如果有扩展实现初始化很耗时，但如果没用上也加
载，会很浪费资源
2. 如果有扩展点加载失败，则所有扩展点无法使用
3. 提供了对扩展点包装的功能(Adaptive)，并且还支持通过set的方式对其他的扩展点进行注入

# 源码剖析

## 1.源码下载、文档

git clone https://gitee.com/mirrors/dubbo.git

**框架设计**

http://dubbo.apache.org/zh-cn/docs/dev/design.html

http://dubbo.apache.org/zh-cn/docs/dev/implementation.html

http://dubbo.apache.org/zh-cn/docs/user/references/registry/zookeeper.html

http://dubbo.apache.org/zh-cn/docs/source_code_guide/export-service.html

http://dubbo.apache.org/zh-cn/docs/source_code_guide/refer-service.html

## 2.架构整体设计

### 2.1调用关系说明



![](image\dubbo-relation.jpg)

在这里主要由四部分组成:
Provider: 暴露服务的服务提供方
Protocol 负责提供者和消费者之间协议交互数据
Service 真实的业务服务信息 可以理解成接口 和 实现
Container Dubbo的运行环境
Consumer: 调用远程服务的服务消费方
Protocol 负责提供者和消费者之间协议交互数据
Cluster 感知提供者端的列表信息
Proxy 可以理解成 提供者的服务调用代理类 由它接管 Consumer中的接口调用逻辑
Registry: 注册中心，用于作为服务发现和路由配置等工作，提供者和消费者都会在这里进行注册
Monitor: 用于提供者和消费者中的数据统计，比如调用频次，成功失败次数等信息。
**启动和执行流程说明:**
提供者端启动 容器负责把Service信息加载 并通过Protocol 注册到注册中心
消费者端启动 通过监听提供者列表来感知提供者信息 并在提供者发生改变时 通过注册中心及时
通知消费端
消费方发起 请求 通过Proxy模块
利用Cluster模块 来选择真实的要发送给的提供者信息
交由Consumer中的Protocol 把信息发送给提供者
提供者同样需要通过 Protocol 模块来处理消费者的信息
最后由真正的服务提供者 Service 来进行处理

### 2.2整体的调用链路

![](image\dubbo-framework.jpg)

整体链路调用的流程:
1. 消费者通过Interface进行方法调用 统一交由消费者端的 Proxy 通过ProxyFactory 来进行代理
对象的创建 使用到了 jdk javassist技术

2. 交给Filter 这个模块 做一个统一的过滤请求 在SPI案例中涉及过

3. 接下来会进入最主要的Invoker调用逻辑
  通过Directory 去配置中读取信息 最终通过list方法获取所有的Invoker
  通过Cluster模块 根据选择的具体路由规则 来选取Invoker列表
  通过LoadBalance模块 根据负载均衡策略 选择一个具体的Invoker 来处理我们的请求
  如果执行中出现错误 并且Consumer阶段配置了重试机制 则会重新尝试执行

4. 继续经过Filter 进行执行功能的前后封装 Invoker 选择具体的执行协议

5. 客户端 进行编码和序列化 然后发送数据

6. 到达Consumer中的 Server 在这里进行 反编码 和 反序列化的接收数据

7. 使用Exporter选择执行器

8. 交给Filter 进行一个提供者端的过滤 到达 Invoker 执行器

9. 通过Invoker 调用接口的具体实现 然后返回

### 2.3源码整体设计

![](image\dubbo-extension.jpg)

**分层介绍:**

- **config 配置层**：对外配置接口，以 `ServiceConfig`, `ReferenceConfig` 为中心，可以直接初始化配置类，也可以通过 spring 解析配置生成配置类

- **proxy 服务代理层**：服务接口透明代理，生成服务的客户端 Stub 和服务器端 Skeleton, 以 `ServiceProxy` 为中心，扩展接口为 `ProxyFactory`

- **registry 注册中心层**：封装服务地址的注册与发现，以服务 URL 为中心，扩展接口为 `RegistryFactory`, `Registry`, `RegistryService`

- **cluster 路由层**：封装多个提供者的路由及负载均衡，并桥接注册中心，以 `Invoker` 为中心，扩展接口为 `Cluster`, `Directory`, `Router`, `LoadBalance`

- **monitor 监控层**：RPC 调用次数和调用时间监控，以 `Statistics` 为中心，扩展接口为 `MonitorFactory`, `Monitor`, `MonitorService`

- **protocol 远程调用层**：封装 RPC 调用，以 `Invocation`, `Result` 为中心，扩展接口为 `Protocol`, `Invoker`, `Exporter`

- **exchange 信息交换层**：封装请求响应模式，同步转异步，以 `Request`, `Response` 为中心，扩展接口为 `Exchanger`, `ExchangeChannel`, `ExchangeClient`, `ExchangeServer`

- **transport 网络传输层**：抽象 mina 和 netty 为统一接口，以 `Message` 为中心，扩展接口为 `Channel`, `Transporter`, `Client`, `Server`, `Codec`

- **serialize 数据序列化层**：可复用的一些工具，扩展接口为 `Serialization`, `ObjectInput`, `ObjectOutput`, `ThreadPool`

### 2.4暴露服务时序

**代码**

```
@startuml
autonumber
participant Actor
participant "ServiceConfig" as sc
participant "ProxyFactory" as pf
participant Invoker
participant Protocol
participant Exporter
participant Transporter
participant Server
participant "ExportListener" as el 
participant Registry
Actor-> sc: export();
sc-> pf: createInvoker();
pf-> Invoker: init();
sc-> Protocol: export();
Protocol-> Exporter: init();
Exporter-> Transporter: bind();
Transporter-> Server: init();
Exporter-> el: exported();
el-> Registry: register();
Actor-> sc: unExport();
sc-> Exporter: unExport();
Exporter->Invoker: destory();
Exporter->el: unExport();
el->Registry: unRegister();
@enduml
```

  **图片**

![](image\服务暴露.svg)

### 2.5引用服务时序

**代码**

```
@startuml
autonumber
participant Actor
participant "ReferenceConfig" as rc
participant "RegistryProtocol" as rp
participant Registry
participant Directory
participant Cluster
participant Protocol
participant Invoker
participant Transporter
participant Client
participant "InvokerListener" as invo
participant "ProxyFactory" as pf
Actor-> rc: get();
rc-> rp: refer();
rp-> Registry: subscribe();
Registry-> Directory: notified();
Directory-> Protocol: refer();
Protocol-> Invoker: init();
Invoker-> Transporter: connect();
Transporter-> Client: init();
Protocol-> invo: referred();
rp-> Cluster: merge();
Cluster->Invoker: init();
rc-> pf: createProxy();
Actor-> rc: destory();
rc-> Invoker: destory();
Invoker->Registry: unsubscribe();
Invoker->Directory: destory();
Invoker->Client: close();
Invoker->invo: destory();
@enduml
```
**图**

![](image\引用服务时序.svg)