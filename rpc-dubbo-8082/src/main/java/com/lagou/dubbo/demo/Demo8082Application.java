package com.lagou.dubbo.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo8082Application {
    public static void main(String[] args) {
        SpringApplication.run(Demo8082Application.class, args);
    }
}
