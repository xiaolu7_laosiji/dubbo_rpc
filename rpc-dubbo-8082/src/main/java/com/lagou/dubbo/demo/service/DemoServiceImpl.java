package com.lagou.dubbo.demo.service;


import org.apache.dubbo.config.annotation.Service;
import org.apache.dubbo.rpc.RpcContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class DemoServiceImpl implements DemoService {

    private Logger log = LoggerFactory.getLogger(DemoService.class);

    @Override
    public void printIp() {
        String ip = (String) RpcContext.getContext().getAttachment("local_ip");
        log.info("客户端请求ip为:" + ip);
    }
}
