package com.lagou.dubbo.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo8081Application {
    public static void main(String[] args) {
        SpringApplication.run(Demo8081Application.class, args);
    }
}
