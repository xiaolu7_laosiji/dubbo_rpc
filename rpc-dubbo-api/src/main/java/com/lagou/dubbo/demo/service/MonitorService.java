package com.lagou.dubbo.demo.service;

public interface MonitorService {
    void methodA();
    void methodB();
    void methodC();
}
