package com.lagou.dubbo.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo8083Application {
    public static void main(String[] args) {
        SpringApplication.run(Demo8083Application.class, args);
    }
}
