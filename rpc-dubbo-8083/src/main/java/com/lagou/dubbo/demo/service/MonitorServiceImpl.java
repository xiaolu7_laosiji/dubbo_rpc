package com.lagou.dubbo.demo.service;

import org.apache.dubbo.config.annotation.Service;

import java.util.Random;

@Service
public class MonitorServiceImpl implements MonitorService {
    @Override
    public void methodA() {
        try {
            Thread.sleep(new Random().nextInt(100));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void methodB() {
        try {
            Thread.sleep(new Random().nextInt(100));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void methodC() {
        try {
            Thread.sleep(new Random().nextInt(100));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
